(in-package #:tetris)

(defvar *pieces*)

(setf *pieces* `((((0 1 0 0)
                   (0 1 0 0)
                   (0 1 0 0)
                   (0 1 0 0)) .
                  ,(cl-colors:as-rgb "8870FF"))
                 (((0 0 0 0)
                   (0 1 1 0)
                   (0 1 1 0)
                   (0 0 0 0)) .
                  ,(cl-colors:as-rgb "FF7416"))
                 (((0 0 0 0)
                   (0 1 1 0)
                   (1 1 0 0)
                   (0 0 0 0)) .
                  ,(cl-colors:as-rgb "FEC606"))
                 (((0 0 0 0)
                   (1 1 0 0)
                   (0 1 1 0)
                   (0 0 0 0)) .
                  ,(cl-colors:as-rgb "E6DCDB"))
                 (((0 0 0 0)
                   (0 1 0 0)
                   (1 1 1 0)
                   (0 0 0 0)) .
                  ,(cl-colors:as-rgb "1DABB8"))
                 (((0 1 1 0)
                   (0 0 1 0)
                   (0 0 1 0)
                   (0 0 0 0)) .
                  ,(cl-colors:as-rgb "E7DF86"))
                 (((0 1 1 0)
                   (0 1 0 0)
                   (0 1 0 0)
                   (0 0 0 0)) .
                  ,(cl-colors:as-rgb "92F22A"))))
