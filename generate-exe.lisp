(load #P"~/quicklisp/setup.lisp")
(push *default-pathname-defaults* asdf:*central-registry*)
(ql:quickload "tetris")
(sb-ext:save-lisp-and-die "tetris" :toplevel #'tetris:main :executable t)
