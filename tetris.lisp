;;;; tetris.lisp

(in-package #:tetris)

(defvar *win-width* 800)
(defvar *win-height* 600)

(defvar *cell-size*)
(setf *cell-size* 20)

(defvar *cell-spacing*)
(setf *cell-spacing* 2)

(defun cell-total-size ()
  (+ *cell-size* *cell-spacing*))

(defvar *field-width*)
(setf *field-width* 12)

(defvar *field-height*)
(setf *field-height* (1- (floor (/ *win-height* (cell-total-size)))))

(defvar *field*)
(defun reset-field ()
  (setf *field* (make-array `(,*field-height* ,*field-width*)
                            :initial-element -1)))

(defvar *x-pos*)

(defun reset-x-pos ()
  (setf *x-pos* (- (/ *field-width* 2) 2)))

(defvar *y-pos*)
(setf *y-pos* 0)

(defvar *timer*)

(defvar *timer-interval*)
(setf *timer-interval* 500)

(defvar *speedup* nil)

(defvar *speedup-interval*)
(setf *speedup-interval* 40)

(defvar *piece-index*)
(setf *piece-index* 0)

(defvar *piece-rotation* 0)

(defun partial (fun &rest args1)
  (lambda (&rest args2)
    (apply fun (append args1 args2))))

(defmacro continuable (&body body)
  "Helper macro that we can use to allow us to continue from an
   error. Remember to hit C in slime or pick the restart so
   errors don't kill the app."
  `(restart-case
       (progn ,@body)
     (continue () :report "Livesupport: Continue")))

(defun debug-log (msg &rest args)
  "Output and flush MSG to STDOUT with arguments ARGS."
  (apply #'format t msg args)
  (finish-output))

(defun test-render-clear (renderer)
  (progn (sdl2:set-render-draw-color renderer 32 0 0 255)
         (sdl2:render-clear renderer)))

(defun set-draw-color (renderer r g b a)
  "Use this function to set the color used for drawing operations."
  (sdl2-ffi.functions:sdl-set-render-draw-color renderer r g b a))

(defun render-rect (renderer rect)
  (sdl2:render-draw-rect renderer rect))

(defun render-filled-rect (renderer rect)
  "Use this function to fill a rectangle on the current rendering target with
the drawing color. "
  (sdl2-ffi.functions:sdl-render-fill-rect renderer rect))

(defun get-piece-pixel (piece x y rot)
  (case rot
    (0 (nth x (nth y piece)))
    (1 (nth (- 3 y) (nth x piece)))
    (2 (nth (- 3 x) (nth (- 3 y) piece)))
    (3 (nth y (nth (- 3 x) piece)))))

(defun mapc-piece (piece rot fn)
  (block nested-loops
    (do ((y 0 (1+ y))) ((= y 4))
      (do ((x 0 (1+ x))) ((= x 4))
        (when (eq 1 (get-piece-pixel piece x y rot))
          (unless (funcall fn x y)
            (return-from nested-loops nil)))))
    t))

(defun render-cell (renderer x y)
  (render-filled-rect renderer
                      (sdl2:make-rect
                       (+ *cell-spacing*
                          (* (cell-total-size) x))
                       (+ *cell-spacing*
                          (* (cell-total-size) y))
                       *cell-size*
                       *cell-size*)))

(defun render-segment (renderer x-pos y-pos x y)
  (render-cell renderer (+ x x-pos) (+ y y-pos)))

(defun check-segment-collisions (x-pos y-pos x y)
  "Returns nil if there is a collision."
  (and (>= (+ x x-pos) 0)
       (< (+ x x-pos) *field-width*)
       (< (+ y y-pos) *field-height*)
       (< (aref *field* (+ y-pos y) (+ x-pos x)) 0)))

(defun piece-count ()
  (list-length *pieces*))

(defun piece-obj (i)
  (nth (mod i (piece-count)) *pieces*))

(defun collides-p (x-pos y-pos rot)
  (let ((piece (car (piece-obj *piece-index*))))
    (not (mapc-piece piece rot
                     (partial #'check-segment-collisions x-pos y-pos)))))

(defun render-field (renderer)
  (set-draw-color renderer 255 255 255 255)
  (render-rect renderer
               (sdl2:make-rect 0 0
                               (+ 2 (* *field-width* (cell-total-size)))
                               (+ 2 (* *field-height* (cell-total-size)))))
  (do ((y 0 (1+ y))) ((= y *field-height*))
    (do ((x 0 (1+ x))) ((= x *field-width*))

      (let ((i (aref *field* y x)))
        (when (>= i 0)
          (let ((color (cdr (piece-obj i))))
            (set-draw-color renderer
                            (floor (* 255 (cl-colors:rgb-red color)))
                            (floor (* 255 (cl-colors:rgb-green color)))
                            (floor (* 255 (cl-colors:rgb-blue color))) 255))
          (render-cell renderer x y ))))))

(defun render-piece (renderer)
  (let* ((piece-obj (piece-obj *piece-index*))
         (piece (car piece-obj))
         (color (cdr piece-obj)))
    (set-draw-color renderer
                    (floor (* 255 (cl-colors:rgb-red color)))
                    (floor (* 255 (cl-colors:rgb-green color)))
                    (floor (* 255 (cl-colors:rgb-blue color))) 255)
    (mapc-piece piece *piece-rotation*
                (partial #'render-segment renderer *x-pos* *y-pos*))))

(defun render (renderer)
  "Render a frame."
  (test-render-clear renderer)
  (render-field renderer)
  (render-piece renderer)
  (sdl2:render-present renderer))

(defun land-segment (piece-index x-pos y-pos x y)
  (setf (aref *field* (+ y-pos y) (+ x-pos x)) piece-index))

(defun land-piece (piece x-pos y-pos rot)
  (mapc-piece (car (piece-obj *piece-index*)) rot
              (partial #'land-segment *piece-index* x-pos y-pos)))

(defun array-slice (arr row)
  (make-array (array-dimension arr 1)
              :displaced-to arr
              :displaced-index-offset (* row (array-dimension arr 1))))

(defun copy-array-row (from-arr from-row
                       to-arr to-row)
  (let ((width (array-dimension from-arr 1)))
    (dotimes (x width)
      (setf (aref to-arr to-row x) (aref from-arr from-row x)))))

(defun clear-full-lines ()
  (loop for y from (1- *field-height*) downto 0
     do (when (every #'(lambda (x) (>= x 0)) (array-slice *field* y))
          (loop for x from 0 to (1- *field-width*)
               do (setf (aref *field* y x) -1)))))

(defun collapse-full-lines ()
  (let ((new-field (make-array `(,*field-height* ,*field-width*)
                               :initial-element -1))
        (dest-y (1- *field-height*)))
    (loop for y from (1- *field-height*) downto 0
       do (when (some #'(lambda (x) (>= x 0)) (array-slice *field* y))
            (copy-array-row *field* y new-field dest-y)
            (setf dest-y (1- dest-y))))
    (setf *field* new-field)))

(defun new-piece ()
  (setf *piece-index* (random (piece-count)))
  (reset-x-pos)
  (setf *y-pos* 0)
  (setf *piece-rotation* 0)
  (when (collides-p *x-pos* *y-pos* *piece-rotation*)
    (init-game)))

(defun handle-tick ()
  (if (collides-p *x-pos* (1+ *y-pos*) *piece-rotation*)
      (progn
        (land-piece (car (piece-obj *piece-index*))
                    *x-pos* *y-pos* *piece-rotation*)
        (clear-full-lines)
        (collapse-full-lines)
        (new-piece)
        t)
      (progn (incf *y-pos*)
             nil)))

(defun rotate (rot)
  (mod (1+ rot) 4))

(defun handle-keydown (keysym)
  (let ((scancode (sdl2:scancode-value keysym)))
    (cond
      ((or (sdl2:scancode= scancode :scancode-escape)
           (sdl2:scancode= scancode :scancode-q))
       (sdl2:push-event :quit))
      ((sdl2:scancode= scancode :scancode-left)
       (sdl2:push-event :move-left-event))
      ((sdl2:scancode= scancode :scancode-right)
       (sdl2:push-event :move-right-event))
      ((sdl2:scancode= scancode :scancode-up)
       (sdl2:push-event :rotate-event))
      ((sdl2:scancode= scancode :scancode-down)
       (sdl2:push-event :speedup-event))
      ((sdl2:scancode= scancode :scancode-space)
       (sdl2:push-event :quickdrop-event)))))

(defun handle-keyup (keysym)
  (let ((scancode (sdl2:scancode-value keysym)))
    (cond
      ((sdl2:scancode= scancode :scancode-down)
       (sdl2:push-event :normal-speed-event)))))

(defun handle-speedup ()
  (sdl2:remove-timer *timer*)
  (init-timer)
  (setf *speedup* t))

(defun handle-normal-speed ()
  (setf *speedup* nil))

(defun handle-quickdrop ()
  (loop until (handle-tick)))

(defun init-events ()
  (sdl2:register-user-event-type :move-left-event)
  (sdl2:register-user-event-type :move-right-event)
  (sdl2:register-user-event-type :rotate-event)
  (sdl2:register-user-event-type :speedup-event)
  (sdl2:register-user-event-type :normal-speed-event)
  (sdl2:register-user-event-type :quickdrop-event)
  (sdl2:register-user-event-type :tick-event))

(defun timer-fun (interval user-arg)
  (sdl2:push-event :tick-event)
  (if *speedup*
      *speedup-interval*
      *timer-interval*))

(cffi:defcallback timer-cb :int ((interval :int) (user-arg :pointer))
  (timer-fun interval user-arg))

(defun init-timer ()
  (setf *timer* (sdl2:add-timer 0 (cffi:callback timer-cb) nil)))

(defun init-game ()
  (reset-field)
  (new-piece))

(defun init ()
  (init-events)
  (init-timer)
  (init-game))

(defun main ()
  "The entry point of the program."
  (sdl2:with-init (:everything)
    (debug-log "Using SDL library version: ~D.~D.~D~%"
               sdl2-ffi:+sdl-major-version+
               sdl2-ffi:+sdl-minor-version+
               sdl2-ffi:+sdl-patchlevel+)
    (sdl2:with-window (win :title "Tetris"
                           :w *win-width* :h *win-height*
                           :flags '(:shown))
      (sdl2:with-renderer (renderer win :flags '(:renderer-accelerated))
        (sdl2:with-event-loop (:method :poll)
          (:initialize
           ()
           (format t "Initializing~%")
           (init))
          (:keydown
           (:keysym keysym)
           (handle-keydown keysym))
          (:keyup
           (:keysym keysym)
           (handle-keyup keysym))
          (:idle
           ()
           (continuable (render renderer)))
          (:move-left-event
           ()
           (unless (collides-p (1- *x-pos*) *y-pos* *piece-rotation*)
             (decf *x-pos*)))
          (:move-right-event
           ()
           (unless (collides-p (1+ *x-pos*) *y-pos* *piece-rotation*)
             (incf *x-pos*)))
          (:rotate-event
           ()
           (unless (collides-p *x-pos* *y-pos* (rotate *piece-rotation*))
             (setf *piece-rotation* (rotate *piece-rotation*))))
          (:speedup-event
           ()
           (handle-speedup))
          (:normal-speed-event
           ()
           (handle-normal-speed))
          (:quickdrop-event
           ()
           (handle-quickdrop))
          (:tick-event
           ()
           (handle-tick))
          (:quit
           () t))))))
