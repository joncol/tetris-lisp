;;;; tetris.asd

(asdf:defsystem #:tetris
    :description "Common Lisp implementation of Tetris"
    :author "Jonas Collberg <jonas.collberg@gmail.com>"
    :license "Specify license here"
    :depends-on (#:cffi
                 #:cl-colors
                 #:sdl2)
    :serial t
    :components ((:file "package")
                 (:file "tetris")
                 (:file "pieces")))
