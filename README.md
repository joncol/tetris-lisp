## Loading from Emacs

    M-x slime
    slime-load-system
    slime-sync-package-and-default-directory

## Loading from REPL

```common-lisp
(asdf:operate 'asdf:load-op 'tetris)
```
